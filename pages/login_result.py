from selenium.webdriver.common.by import By

class IntramLoginResult:

    # Locators
    username = (By.XPATH, '/html/body/div/div/main/div/div[2]/div/div/nav/div[1]/div/div[2]/span[1]')
    error_box_email = (By.XPATH, '/html/body/div/div/main/div/div[2]/div/div/div/div/div[2]/div/div/form/div[2]/span')
    error_box_password = (By.XPATH, '/html/body/div/div/main/div/div[2]/div/div/div/div/div[2]/div/div/form/div[3]/span')
    email_nonexistent = (By.XPATH, '//*[@id="text-error-box"]')


    # Initializer
    def __init__(self, browser):
        self.browser = browser

    
    # Interaction Methods
    def username_value(self):
        username = self.browser.find_element(*self.username)
        value = username.get_attribute('title')
        return value

    def invalid_mail_value(self):
        invalid_mail = self.browser.find_element(*self.error_box_email)
        value = invalid_mail.get_attribute('class')
        return value

    def email_nonexistent_value(self):
        email_nonexistent = self.browser.find_element(*self.email_nonexistent)
        value = email_nonexistent.get_attribute('class')
        return value

    def required_email_value(self):
        required_mail = self.browser.find_element(*self.error_box_email)
        value = required_mail.get_attribute('class')
        return value

    def required_password_value(self):
        required_password = self.browser.find_element(*self.error_box_password)
        value = required_password.get_attribute('class')
        return value
    

    def title(self):        
        return self.browser.title