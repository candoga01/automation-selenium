from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class IntramLoginPage:

    #url
    url = 'https://account.intram.org/login'

    #locators
    EMAIL_INPUT = (By.ID, 'email')
    PASSWORD_INPUT = (By.ID, 'password')
    LOGIN_BUTTON = (By.XPATH, '/html/body/div/div/main/div/div[2]/div/div/div/div/div[2]/div/div/form/div[5]/button')

    # Initializer
    def __init__(self, browser):
        self.browser = browser

    # Interaction methods

    def load(self):
        self.browser.get(self.url)

    def login(self, email, password):
        email_input = self.browser.find_element(*self.EMAIL_INPUT)
        email_input.send_keys(email)

        password_input = self.browser.find_element(*self.PASSWORD_INPUT)
        password_input.send_keys(password)
        
        login_button = self.browser.find_element(*self.LOGIN_BUTTON)
        login_button.click()
